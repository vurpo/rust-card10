#![no_std]
#![no_main]

use card10_l0dable::*;
use core::fmt::{self, Write};
//use cichlid::math::trig::*;
use mish::*;

const model: [((f32,f32,f32),(f32,f32,f32));12] = [
((-1.0, 1.0,-1.0),( 1.0, 1.0,-1.0)),
(( 1.0, 1.0,-1.0),( 1.0, 1.0, 1.0)),
(( 1.0, 1.0, 1.0),(-1.0, 1.0, 1.0)),
((-1.0, 1.0, 1.0),(-1.0, 1.0,-1.0)),

((-1.0,-1.0,-1.0),( 1.0,-1.0,-1.0)),
(( 1.0,-1.0,-1.0),( 1.0,-1.0, 1.0)),
(( 1.0,-1.0, 1.0),(-1.0,-1.0, 1.0)),
((-1.0,-1.0, 1.0),(-1.0,-1.0,-1.0)),

((-1.0, 1.0,-1.0),(-1.0,-1.0,-1.0)),
(( 1.0, 1.0,-1.0),( 1.0,-1.0,-1.0)),
(( 1.0, 1.0, 1.0),( 1.0,-1.0, 1.0)),
((-1.0, 1.0, 1.0),(-1.0,-1.0, 1.0))];

// from cichlid and FastLED
pub fn sin16(theta: u16) -> i16 {
    static BASE: [u16; 8] = [0, 6393, 12539, 18204, 23170, 27245, 30273, 32137];
    static SLOPE: [u8; 8] = [49, 48, 44, 38, 31, 23, 14, 4];
    let mut offset = (theta & 0x3fff) >> 3;
    if (theta & 0x4000) != 0 {
        offset = 2047 - offset;
    }

    let section: u8 = (offset / 256) as u8;
    let b: u16 = unsafe { *BASE.get_unchecked(section as usize) };
    let m: u16 = u16::from(unsafe { *SLOPE.get_unchecked(section as usize) });

    let secoffset8: u8 = (offset as u8) / 2;
    let mx: u16 = m * u16::from(secoffset8);
    let mut y: i16 = (mx + b) as i16;
    if (theta & 0x8000) != 0 {
        y = -y;
    }
    y
}

fn to_screen_coord(coord: (f32,f32,f32)) -> (u16,u16) {
    let z: f32 = (coord.2+2.5)/20.0;
    let x: u16 = ((coord.0/z) as u16)+80;
    let y: u16 = ((coord.1/z) as u16)+40;
    (x, y)
}

fn translate(coord: (f32,f32,f32), t: (f32,f32,f32)) -> (f32,f32,f32) {
    (coord.0+t.0, coord.1+t.1, coord.2+t.2)
}

fn rotatey(coord: (f32,f32,f32), t: f32) -> (f32,f32,f32) {
    let s: f32 = sin(t);
    let c: f32 = cos(t);
    (coord.0*c-coord.2*s, coord.1, coord.0*s+coord.2*c)
}

main!(main);
fn main() {
    let result = run();
    if let Err(error) = result {
        writeln!(UART, "error: {}\r", error).unwrap();
    }
}

fn run() -> Result<(), Error> {
    let display = Display::open();

    let start: MilliSeconds = MilliSeconds::time();

    loop {
        //display.clear(Color::black());

        let time: u16 = ((MilliSeconds::time().0-start.0) as u16);
        /*let offset: f32 = f32::sin(time)+0.5;

        for line in &model {
            let c1 = to_screen_coord(rotatey(translate(line.0, (0.0,offset,0.0)),time));
            let c2 = to_screen_coord(rotatey(translate(line.1, (0.0,offset,0.0)),time));
            //display.line(c1.0, c1.1, c2.0, c2.1, Color::white(), LineStyle::Full, 1);
        }*/

        let mut framebuffer = display.framebuffer();

        framebuffer.clear(framebuffer::RawColor::black());
        
        let string = &"hello world - I am vurpo - vurpo.fi";
        let text_width = string.len() as isize *15;
        let text_offset = (160 as isize)-((time/32) as isize)%(160+text_width);

        for i in 0..string.len() {

            let x = text_offset+(i as isize*15);
            let y = (sin16(x as u16 *452)/2048) as isize +28;

            let mut text = framebuffer.text(
                x,
                y,
                &framebuffer::Font::font24(),
                framebuffer::RawColor::white());

            write!(text, "{}", string.chars().nth(i).unwrap_or(' '));
        }

        //let mut text = framebuffer.text(0,0,&framebuffer::Font::font20(), framebuffer::RawColor::white());
        //write!(text, "{}", sin16(text_offset as u16*128));

        framebuffer.send();

        //display.update();
    }
}

// -----------------------------------------------------------------------------
// Error
// -----------------------------------------------------------------------------

#[derive(Debug)]
pub enum Error {
    UartWriteFailed(fmt::Error),
    SensorInteractionFailed(BHI160Error),
}

impl From<fmt::Error> for Error {
    fn from(error: fmt::Error) -> Self {
        Error::UartWriteFailed(error)
    }
}

impl From<BHI160Error> for Error {
    fn from(error: BHI160Error) -> Self {
        Error::SensorInteractionFailed(error)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::UartWriteFailed(error) => error.fmt(f),
            Error::SensorInteractionFailed(error) => error.fmt(f),
        }
    }
}
